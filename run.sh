#!/bin/sh

while :
do
  date
  echo "--- Checking Inventory"

  python3 /ui-inventory-check/ui-inventory-check.py
  RET=$?
  if [ ${RET} -ne 0 ];
  then
    echo "Exit status not 0"
    echo "Sleep 120"
    sleep 120
  fi
  date
  echo "Sleep $SLEEP"
  sleep $SLEEP
done
