UI Inventory Checker
======
Build Status:
------
[![pipeline status](https://gitlab.com/calebcall/ui-inventory-checker/badges/master/pipeline.svg)](https://gitlab.com/calebcall/ui-inventory-checker/commits/master)

Using this Image:
------
Running the container
A typical invocation of the container might be:

```
$ docker run -e DISCORD_WEBHOOK=https://discord.com/api/webhooks/939405697812742110/kjhsdflsdfgljkhaskjhasdkjsyvKJHSDFK8GHkjgGHJkkjh \
      -e SLACK_WEBHOOK=https://hooks.slack.com/services/KJHUYTG80/BkasdJkjsgf0/H12jksdfKJvkjsd \
      -e ACCOUNT_SID=ACkkjsd98jd73hfpa0dkuaas9nv8smd \
      -e AUTH_TOKEN=b44snNYVxNQ2nkKXrg7oE7wZjkiGiwXmc \
      -e TWILIO_NUMBER=+16239877782 \
      -e CELL_NUMBER="+16509985634" \
      -e PRODUCT_HANDLES="uvc-g4-doorbell, udm-pro" \
      -e SLEEP=30 \
      --name ui-inventory-checker \
      calebcall/ui-inventory-checker:latest
```

The following environment variables can be used:
  - `DISCORD_WEBHOOK`: Set this to the webhook for your Discord channel (**default:** `<blank>`)
  - `SLACK_WEBHOOK`: Set this to the webhook for your Slack channel (**default:** `<blank>`)
  - `ACCOUNT_SID`: Set this to your Twilio Account SID (**default:** `<blank>`)
  - `AUTH_TOKEN`: Set this to your Twilio Authentication Token (**default:** `<blank>`)
  - `TWILIO_NUMBER`: Set this to your Twilio assigned number (**default:** `<blank>`)
  - `CELL_NUMBER`: Set this to your cell phone number you want to be texted when products come in-stock (**default:** `<blank>`)
    `PRODUCT_HANDLES`: Set this to the product handles from https://store.ui.com that you would like to monitor (**default:** `"udm-pro", "uvc-g4-doorbell"`)
  - `SLEEP`: Set this to the amount of time (in seconds) you want it to wait between polls (**default:** `30`)

All of the above environment variables are *optional*, if not defined then it will not be used.  You should enable at least one of the notifications (Discord, Slack, or Twilio)

Todo:
------
  -
