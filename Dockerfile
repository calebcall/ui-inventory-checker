FROM alpine:latest

ENV PYTHONPATH /usr/lib/python3.6/site-packages

RUN apk add --update python3 python3-dev musl-dev expat gcc py3-pip &&\
    python3 -m pip install --upgrade pip &&\
    mkdir /ui-inventory-check &&\
    python3 -m pip install twilio slack_webhook discord.py &&\
    adduser -D ui-bot

COPY ui-inventory-check.py /ui-inventory-check/
COPY run.sh /ui-inventory-check/

RUN chmod +x /ui-inventory-check/*

USER ui-bot

CMD ["/ui-inventory-check/run.sh"]
