#!/usr/bin/env python3

import requests
import json
import os


### Configs Section
### Each notification channel is optional, but you should configure at least one
### UI products in the product handle taken from the product url

## Discord config
discord_webhook = os.getenv('DISCORD_WEBHOOK', '') #Discord webhook

## Slack Webhook
slack_webhook = os.getenv('SLACK_WEBHOOK', '') # Slack webhook

##Twilio Config
account_sid = os.getenv('ACCOUNT_SID', '') # twilio account sid https://support.twilio.com/hc/en-us/articles/223136607-What-is-an-Application-SID-
auth_token = os.getenv('AUTH_TOKEN', '') # twilio auth token https://www.twilio.com/docs/iam/access-tokens
twilio_number = os.getenv('TWILIO_NUMBER', '') # twilio phone number given to you
text_to_cells = os.getenv('CELL_NUMBER', '').replace(' ', '') # replace with your cell number, can be a list of numbers to send to

##UI Products to check
product_list = os.getenv('PRODUCT_HANDLES', 'udm-pro,uvc-g4-doorbell').replace(' ', '')

######## No Config below this ########

def checkStatus():
# Get status of product
    collections_to_check = []
    collections_list = requests.get('https://store.ui.com/collections.json')
    collections = json.loads(collections_list.text)['collections']
    for collection in collections:
        collections_to_check.append(collection['handle'])
    product_info = []
    product_handles = []
    products_to_check = product_list.split(',')
    for i in products_to_check:
        product_handles.append(i)
    for product_page in collections_to_check:
        page = requests.get("https://store.ui.com/collections/%s/products.json" % (product_page))
        products = json.loads(page.text)['products']
        for product in products:
            product_info.append(product)
    page = requests.get("https://store.ui.com/products.json")
    products = json.loads(page.text)['products']
    for product in products:
        product_info.append(product)

    unique_product_list = []
    for x in product_info:
        if x not in unique_product_list:
            unique_product_list.append(x)

    if not (discord_webhook) and not (account_sid) and not (slack_webhook):
        print("No notification channels enabled!")
        notifications = 'disabled'
    else:
        notifications = 'enabled'
    for handle in product_handles:
        print("Checking %s" % (handle))
        for product in unique_product_list:
            if handle in product['handle'] and product['variants'][0]['available'] == True:
                if notifications == 'disabled':
                    sendStdout(product)
                else:
                    if discord_webhook:
                        sendDiscord(product)
                    if slack_webhook:
                        sendSlack(product)
                    if account_sid:
                        sendTwilio(product)


def sendTwilio(data):
    from twilio.rest import Client

    text_to_number = []
    cells_to_text = text_to_cells.split(',')
    for i in cells_to_text:
        text_to_number.append(i)

    client = Client(account_sid, auth_token)
    for number in text_to_number:
        message = client.messages \
            .create(
                body="%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']),
                from_=twilio_number,
                to=number,
                         )


def sendDiscord(data):
    from discord import Webhook, RequestsWebhookAdapter

    dis_id = int(discord_webhook.split('/')[5])
    dis_token = discord_webhook.split('/')[6]
    webhook = Webhook.partial(dis_id, dis_token, adapter=RequestsWebhookAdapter())
    webhook.send("%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']), username='UI Stock Bot')

def sendSlack(data):
    from slack_webhook import Slack

    slack = Slack(url=slack_webhook)
    slack.post(text="%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']),
        username="UI Stock Bot",
        )

def sendStdout(data):
    print("%s is now in-stock!\nhttps://store.ui.com/products/%s" % (data['title'], data['handle']))

if __name__ == '__main__':
    checkStatus()
